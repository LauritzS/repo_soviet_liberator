/// @description Movement, grv, shooting
vsp = vsp + grv;





//Horizontal Collision
if (place_meeting(x + hsp, y, obj_wall))
{
	while (!place_meeting(x + sign(hsp), y, obj_wall))
	{
		x = x + sign(hsp);
	}
	
	hsp = -hsp;
	
}

x = x + hsp;


//Vertical Collision
if (place_meeting(x , y + vsp, obj_platform))
{
	while (!place_meeting(x , y + sign(vsp), obj_platform))
	{
		y = y + sign(vsp);
	}
	
	vsp = 0;
}
 
y = y + vsp;


// Animation
if (place_meeting(x, y + 1, obj_platform))
{
	
	sprite_index = spr_sovietflamemaster_f1; 
	image_speed = 1;
	//if (sign(vsp) > 0) image_index = 1; else image_index = 0;
}
else
{
	
	image_speed = 1;
	if (hsp == 0)
	{
		sprite_index = spr_sovietflamemaster_f1; //Idle
	}
	else
	{
		sprite_index = spr_sovietflamemaster_f1;
	}
}



// Turning the player sprite
if (hsp != 0) image_xscale = sign(hsp) * 1; 
//Sign returns either 1 or -1, depending on if something is true or not. 
//In this case it can do the work of flipping the sprite for us.
//And because I scaled up the player sprite in the create event by 2, it has to be multiplied again

 //Detecting and shooting
 if (point_distance(obj_player.x, obj_player.y, x, y) < 600)
 {
	
	countdown--;
	if (countdown <= 0)
	{
		countdown = countdownrate;
		if (obj_player.x < x)
		{
			with (instance_create_layer(obj_enemy.x-15, obj_enemy.y, "Bullets", obj_E_bullet) /*&&*/ ) hspeed = -9;
			image_speed = 0.05;
		}

		if (obj_player.x > x )
		{
			with (instance_create_layer(obj_enemy.x+15, obj_enemy.y, "Bullets", obj_E_bullet)) hspeed = 9;
			image_speed = 0.05;
		}
	}
 } 





















