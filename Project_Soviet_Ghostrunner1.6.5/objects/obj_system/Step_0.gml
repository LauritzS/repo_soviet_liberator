// Progress the transition
if (mode != TRANS_MODE.OFF)
{
	if (mode == TRANS_MODE.INTRO)
	{
		percent = max(0, percent-0.05);
	}
	else
	{
		percent = min(percenttarget, percent+0.05);
	}
	
	if (percent == percenttarget) || (percent == 0)
	{
		switch (mode)
		{
			case TRANS_MODE.INTRO:
			mode = TRANS_MODE.OFF;
				break;
		
			case TRANS_MODE.NEXT:
			mode = TRANS_MODE.INTRO;
			room_goto_next();
				break;
				
			case TRANS_MODE.GOTO:
			mode = TRANS_MODE.INTRO;
			room_goto(target);
				break;
				
			case TRANS_MODE.RESTART:
			game_restart();
				break;
				
		}
	}
}

// Room checker
if (keyboard_check_pressed(vk_enter) || gamepad_button_check_pressed(0,gp_face2) || gamepad_button_check_pressed(4,gp_face2))
{
	switch(room)
	{
		case rm_transition1:
		case rm_controls:
			SlideTransition(TRANS_MODE.GOTO, rm_menu);
			break;
		
		case rm_victory:
			game_restart();
			break;	
	}
}

if ((rm_cave || rm_ice) && (keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(0,gp_start) || gamepad_button_check_pressed(0,gp_start)))
{
	SlideTransition(TRANS_MODE.GOTO, rm_menu);
}
