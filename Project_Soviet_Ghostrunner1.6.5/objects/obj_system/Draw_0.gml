switch(room)
{
	case rm_menu:
	draw_set_halign(fa_center);
	var c = c_red;
	var d = c_yellow;
		draw_text_transformed_color(room_width/2, 253, 
		"SOVIET RUNNER", 2, 2, 0, c,c,c,c, 1);
		
		draw_text_transformed_color(room_width/2, 500, 
		@"Privet comrade to the Soviet Runner Playtesting experience!
Try to exterminate all hostiles in the area as fast as possible by jumping and shooting.

Navigate trough the menu with arrow keys and enter 
or gamepad and the 'O'/ 'B' button (Playstation or Xbox).
Uspekhov!", 0.7, 0.7, 0, d,d,d,d, 1);
		
		draw_set_halign(fa_left);
	break;
	
	case rm_transition1:
	draw_set_halign(fa_center);
	var c = c_red;
	var d = c_yellow;
		draw_text_transformed_color(room_width/2, 253, 
		"You won!", 2, 2, 0, c,c,c,c, 1);
		
		draw_text_transformed_color(room_width/2, room_height/2, 
		"You did it comrade! Your time was: ---", 1, 1, 0, d,d,d,d, 1);
		draw_text_transformed_color(room_width/2, 512, 
		"Press ENTER or 'O'/'B'  on your gamepad", 1, 1, 0, d,d,d,d, 1);
		draw_set_halign(fa_left);
	break;
	
	case rm_controls:
	draw_set_halign(fa_center);
	var c = c_red;
	var d = c_yellow;
	
	draw_text_transformed_color(room_width/2, 253, 
		"Controls", 2, 2, 0, c,c,c,c, 1);
		
		draw_text_transformed_color(room_width/2, 550, 
		@"Keyboard:
Arrow Keys / 'A' and 'D' Keys = Left/Right Movement
Enter Key = Shooting
'W' / Space / 'Up' Keys = Jump
ESC Key = Return to Menu

Gamepad:
Left Stick = Left/Right Movement
'X' / 'A' = Jumping (on Playstation or Xbox Gamepad)
Right Shoulder Trigger = Shooting
Start Button = Return to Menu", 0.7, 0.7, 0, d,d,d,d, 1);
		draw_set_halign(fa_left);
	break;
	
	case rm_victory:
	draw_set_halign(fa_center);
	var d = c_yellow;
		draw_text_transformed_color(room_width/2, room_height/4, 
		@"Congrats comrade, you did it!
		
		Come home to celebrate with lots of vodka!
		
		... What? You want to play again? Okay, press ENTER then", 2, 2, 0, d,d,d,d, 1);
		draw_set_halign(fa_left);
	break;
	
}	