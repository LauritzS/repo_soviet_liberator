/// @description Update Camera

// Update destination
if (instance_exists(follow))
{
	//xTo = follow.x;
	yTo = follow.y;
}


// Update movement towards object
//x += (xTo - x) / 25; // Divides the current position of the view with the object. For smooth camera movement
y += (yTo - y) / 25;

// Camera clamp
x = clamp(x, view_width_half, room_width - view_width_half);
y = clamp(y, view_height_half, room_height - view_height_half);

// Update camera view on player
camera_set_view_pos(cam, x - view_width_half, y - view_height_half); 
	//Repositions the camera view so the player is in the middle




